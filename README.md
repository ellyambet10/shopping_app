# README

This is is a simple shopping app built in vue.js

It's built to show:
		
		* Vue Instance
		* Attribute Binding
		* Conditional Rendering
		* List Rendering
		* Event Handling
		* Class & Style Binding
		* Computed properties
			*************
		* Components
		* Communicating Events
		* Forms
		* Tabs
